
package br.com.fatec.cadastro.business;


import java.util.List;

import br.com.fatec.cadastro.model.Curso;
import br.com.fatec.cadastro.services.CursoService;

public class CursoBusiness {
	
	
	public enum MensagemEnum { SUCESSO, ERROR };
	private String msg;

	public MensagemEnum salvarCurso(Curso curso)
	{

	    MensagemEnum retorno;
		
		if(curso == null){
			msg= "br.com.fatec.cadastro.business.cursoNullError";
			retorno = MensagemEnum.ERROR;
		}
		else
		{
			if(curso.getNome().length() < 3)
			{
				msg = "br.com.fatec.cadastro.business.cursoSizeError";
				retorno = MensagemEnum.ERROR;
			}
			else
			{
				CursoService cursoService = new CursoService();
				   if(cursoService.save(curso))
				   {
				      retorno = MensagemEnum.SUCESSO;
				      msg =  "br.com.fatec.cadastro.business.cursoSucesso";
				   }
				   else
				   {
					  retorno = MensagemEnum.ERROR;
					  msg =  "br.com.fatec.cadastro.business.erroBanco";
				   }
			}
		}
		
	   return retorno;		
			
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public List<Curso> getAllCursos() {
		 
		CursoService cursoService = new CursoService();
		return cursoService.getAllCursos();
	}

}
