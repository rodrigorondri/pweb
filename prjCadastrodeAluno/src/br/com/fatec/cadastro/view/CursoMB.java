

package br.com.fatec.cadastro.view;


import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import br.com.fatec.cadastro.business.CursoBusiness;
import br.com.fatec.cadastro.business.CursoBusiness.MensagemEnum;
import br.com.fatec.cadastro.model.Curso;
import br.com.fatec.cadastro.util.JSFUtil;

@ManagedBean
@ViewScoped
public class CursoMB implements Serializable{

	private static final long serialVersionUID = 1L;
	private Curso curso = new Curso();
	CursoBusiness cb = new CursoBusiness();

	public Curso getCurso() {
		return curso;
	}

	public void setCurso(Curso curso) {
		this.curso= curso;
	}

	public void salvar() {
			
		if(cb.salvarCurso(curso) == MensagemEnum.SUCESSO){
			System.out.println("Sucesso!");	
			curso = new Curso();
		}
		else{
			System.out.println("Erro!");
		}
		
		JSFUtil.addMessage("br.com.fatec.cadastro.controller.CursoMB.idMensagem",cb.getMsg());
		
		
	}
	
	public List<Curso> getCursos()
	{
		 return cb.getAllCursos();
		
	}

}
