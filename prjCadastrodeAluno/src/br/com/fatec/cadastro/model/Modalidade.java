package br.com.fatec.cadastro.model;

public enum Modalidade {
	BACHARELADO("Bacharelado"),
	TECNOLOGIA("Tecnologia"),
	TECNICO("T�cnico");
	
	private String label;
	
	private Modalidade(String label){
		this.label = label;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
	
}
