package br.com.fatec.cadastro.view;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import br.com.fatec.cadastro.model.Modalidade;

@ManagedBean
@ApplicationScoped
public class ModalidadeMB {
	
	private Modalidade selectedModalidade;
	
	
	
	public Modalidade getSelectedModalidade() {
		return selectedModalidade;
	}



	public void setSelectedModalidade(Modalidade selectedModalidade) {
		this.selectedModalidade = selectedModalidade;
	}



	public Modalidade[] getModalidades(){
		return Modalidade.values();
	}
}
