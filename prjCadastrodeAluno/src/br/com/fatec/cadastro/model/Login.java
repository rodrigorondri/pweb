package br.com.fatec.cadastro.model;

import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

public class Login {
	long id;
	String login;
	String Senha;
	
	@OneToOne
	@JoinColumn(name="id_perfil")
	private Perfil perfil;
	
	
	
	public Perfil getPerfil() {
		return perfil;
	}
	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getSenha() {
		return Senha;
	}
	public void setSenha(String senha) {
		Senha = senha;
	}
	
	
	
	
}
