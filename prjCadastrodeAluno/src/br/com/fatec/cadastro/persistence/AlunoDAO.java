

package br.com.fatec.cadastro.persistence;

import java.util.List;

import javax.persistence.EntityManager;


import br.com.fatec.cadastro.model.Aluno;

public class AlunoDAO  extends GenericDAO<Integer, Aluno>{

	

	public AlunoDAO(EntityManager entityManager) {
	    super(entityManager);
	}
	
	public boolean findByRa (Aluno aluno){
		boolean resultado = false;
		if(this.entityManager.find(Aluno.class, aluno.getRa()) != null){
			resultado = true;// tem ra igual
		}
		return resultado;
	}

	public List<Aluno> findBycpf(Aluno aluno) {
		
    	return this.entityManager.createQuery("SELECT a FROM Aluno a WHERE a.cpf = :cpf").getResultList();
    }
	
	/*
	@NamedQuery(name="Aluno.findByCpf", query="SELECT a FROM Aluno a WHERE a.cpf = :cpf"),
	@NamedQuery(name="Aluno.findByRg", query="SELECT a FROM Aluno a WHERE a.rg = :rg"), */
}

 


