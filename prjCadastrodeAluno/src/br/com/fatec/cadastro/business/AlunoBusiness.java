
package br.com.fatec.cadastro.business;


import java.util.List;

import br.com.fatec.cadastro.model.Aluno;
import br.com.fatec.cadastro.services.AlunoService;

public class AlunoBusiness {
	
	
	public enum MensagemEnum { SUCESSO, ERROR };
	private String msg;

	public MensagemEnum salvarAluno(Aluno aluno)
	{

	    MensagemEnum retorno;
		
		if(aluno == null){
			msg= "br.com.fatec.cadastro.business.alunoNullError";
			retorno = MensagemEnum.ERROR;
		}
		else
		{	

			if(aluno.getRa().length() < 6)
			{
				msg = "br.com.fatec.cadastro.business.alunoRaError";
				retorno = MensagemEnum.ERROR;
			}
			else
			{	AlunoService alunoService = new AlunoService();// recente
				if(alunoService.getRa(aluno) == true){
					
					msg = "br.com.fatec.cadastro.business.alunoRafound";
					retorno = MensagemEnum.ERROR;
					
				}
				else
				{
							if(aluno.getNome().length() < 5)
							{
								msg = "br.com.fatec.cadastro.business.alunoSizeError";
								retorno = MensagemEnum.ERROR;
							}
							else
							{
								if(aluno.getNome().split(" ").length < 2)
								{
									msg = "br.com.fatec.cadastro.business.alunoSobrenomeError";
									retorno = MensagemEnum.ERROR;
								}
								else
								{
									   
									   
									   alunoService = new AlunoService();
									   if(alunoService.save(aluno))
									   {
									      retorno = MensagemEnum.SUCESSO;
									      msg =  "br.com.fatec.cadastro.business.alunoSucesso";
									   }
									   else
									   {
										  retorno = MensagemEnum.ERROR;
										  msg =  "br.com.fatec.cadastro.business.erroBanco";
									   }
								}
							}
						}
				}	
			}
		
	   return retorno;		
			
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public List<Aluno> getAllAlunos() {
		 
		AlunoService alunoService = new AlunoService();
		return alunoService.getAllAlunos();
	}

}
