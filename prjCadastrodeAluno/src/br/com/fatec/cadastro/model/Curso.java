package br.com.fatec.cadastro.model;

import java.util.ArrayList;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import br.com.fatec.cadastro.model.*;




@Entity
public class Curso {
	@Id
	private int codigoCur;
	private String nome;
	private int duracao;
	//private String modalidade;
	
	@Enumerated(EnumType.STRING)
	private Modalidade modalidade;
	
	@javax.persistence.Transient	
	private ArrayList <Aluno> alunos;
	
	

	public int getCodigoCur() {
		return codigoCur;
	}

	public void setCodigoCur(int codigoCur) {
		this.codigoCur = codigoCur;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getDuracao() {
		return duracao;
	}

	public void setDuracao(int duracao) {
		this.duracao = duracao;
	}


	public ArrayList<Aluno> getAlunos() {
		return alunos;
	}

	public void setAlunos(ArrayList<Aluno> alunos) {
		this.alunos = alunos;
	}

	public Modalidade getModalidade() {
		return modalidade;
	}

	public void setModalidade(Modalidade modalidade) {
		this.modalidade = modalidade;
	}
	
	
	
	
	
	

}
