package br.com.fatec.cadastro.model;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;



@Entity

/* no futuro talvez
@NamedQueries({
	@NamedQuery(name="Aluno.findByCpf", query="SELECT a FROM Aluno a WHERE a.cpf = :cpf"),
	@NamedQuery(name="Aluno.findByRg", query="SELECT a FROM Aluno a WHERE a.rg = :rg"),
}) */

public class Aluno {
	@Id
	private String ra;
	private String rg;
	private String cpf;
	private String nome;
	private String endereco;
	private String dataNasc;
	
	@ManyToMany()
	private  Collection<Curso> cursos = new ArrayList<Curso>();
	
	@OneToMany(cascade=CascadeType.ALL)
	@JoinTable(name="alunodisciplina",joinColumns=@JoinColumn(name="aluno_ra"), 
	inverseJoinColumns=@JoinColumn(name="disciplina_codigo"))
	private Collection <Disciplina> disciplinas = new ArrayList <Disciplina>();
	
	
	public void addDisciplina(Disciplina disciplina){
		disciplinas.add(disciplina);
	}
	
	public Collection<Disciplina> getDisciplinas() {
		return disciplinas;
	}
	
	public void setDisciplinas(Collection<Disciplina> disciplinas) {
		this.disciplinas = disciplinas;
	}

	public String getRa() {
		return ra;
	}
	public void setRa(String ra) {
		this.ra = ra;
	}
	public String getRg() {
		return rg;
	}
	public void setRg(String rg) {
		this.rg = rg;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public String getDataNasc() {
		return dataNasc;
	}
	public void setDataNasc(String dataNasc) {
		this.dataNasc = dataNasc;
	}

	public Collection<Curso> getCursos() {
		return cursos;
	}

	public void setCursos(Collection<Curso> cursos) {
		this.cursos = cursos;
	}
	

}
