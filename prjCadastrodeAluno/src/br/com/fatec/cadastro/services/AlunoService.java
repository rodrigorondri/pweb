

package br.com.fatec.cadastro.services;


import java.util.List;



import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.fatec.cadastro.model.Aluno;
import br.com.fatec.cadastro.persistence.AlunoDAO;
import br.com.fatec.cadastro.persistence.MyEntityManager;

public class AlunoService {
	
	private AlunoDAO alunoDAO;
	private MyEntityManager myEntityManager;
	
	
	public AlunoService()
	{
		this.myEntityManager = new MyEntityManager();
		alunoDAO = new AlunoDAO(myEntityManager.getEntityManager());
		
	}
	
	public boolean getRa(Aluno aluno){
		boolean resultado = false;
		
		if(alunoDAO.findByRa(aluno) == true){
			resultado = true; // ra repetido
		}
		
		return resultado;
		
	}
	
	/*public boolean byCpf(Aluno aluno){
		boolean resultado = false;
		
		try{
			
			if (alunoDAO.findBycpf(aluno) == null){
				
			}

		}catch (Exception e){
			e.printStackTrace();
		}
		return resultado;
		
	}*/
	
	 public boolean save(Aluno aluno){
		    boolean retorno = false;
		    
	        try{
	            myEntityManager.beginTransaction();
	            alunoDAO.save(aluno);
	            myEntityManager.commit();
	            retorno = true;
	        }catch(Exception e){
	            e.printStackTrace();
	            myEntityManager.rollBack();
	            
	        }
	        finally
	        {
	        	myEntityManager.close();
	        	
	        }
	        
	        return retorno;
	    }
	 
	 
	public List<Aluno> getAllAlunos() {
		
		return alunoDAO.findAll(Aluno.class);
		
	}
	
	/*met�do em teste
	@SuppressWarnings("unchecked")
	public List<Aluno> getFindCpf(Aluno aluno){
		
		Query query = ((EntityManager) myEntityManager).createNamedQuery("Aluno.findByCpf");
		query.setParameter("cpf", aluno.getCpf());
		
		return query.getResultList();
	} */


}









