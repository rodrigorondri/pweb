package br.com.fatec.cadastro.services;

import java.util.List;

import br.com.fatec.cadastro.model.Disciplina;
import br.com.fatec.cadastro.persistence.DisciplinaDAO;
import br.com.fatec.cadastro.persistence.MyEntityManager;

public class DisciplinaService {
	private DisciplinaDAO disciplinaDAO;
	private MyEntityManager myEntityManager;
	
	
	public DisciplinaService()
	{
		this.myEntityManager = new MyEntityManager();
		disciplinaDAO = new DisciplinaDAO(myEntityManager.getEntityManager());
		
	}
	
	 public boolean save(Disciplina disciplina){
		    boolean retorno = false;
		    
	        try{
	            myEntityManager.beginTransaction();
	            disciplinaDAO.save(disciplina);
	            myEntityManager.commit();
	            retorno = true;
	        }catch(Exception e){
	            e.printStackTrace();
	            myEntityManager.rollBack();
	            
	        }
	        finally
	        {
	        	myEntityManager.close();
	        	
	        }
	        
	        return retorno;
	    }

	public List<Disciplina> getAllDisciplinas() {
		
		return disciplinaDAO.findAll(Disciplina.class);
		
	}

}
