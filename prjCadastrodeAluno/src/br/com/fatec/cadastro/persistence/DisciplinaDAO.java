package br.com.fatec.cadastro.persistence;

import javax.persistence.EntityManager;

import br.com.fatec.cadastro.model.Disciplina;

public class DisciplinaDAO extends GenericDAO<Integer, Disciplina>{


		public DisciplinaDAO(EntityManager entityManager) {
		    super(entityManager);
		}

}
