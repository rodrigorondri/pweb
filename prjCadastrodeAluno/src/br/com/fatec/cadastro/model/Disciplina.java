package br.com.fatec.cadastro.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Disciplina {
	@Id
	private int codigoDis;
	private String nome;
	private int cargaHr;
	
	
	
	/* Temporariamente desativada
	public Disciplina(int codigo, String nome, int cargaHr) {
		super();
		this.codigoDis = codigo;
		this.nome = nome;
		this.cargaHr = cargaHr;
	}
	*/
	

	public String getNome() {
		return nome;
	}
	public int getCodigoDis() {
		return codigoDis;
	}
	public void setCodigoDis(int codigoDis) {
		this.codigoDis = codigoDis;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getCargaHr() {
		return cargaHr;
	}
	public void setCargaHr(int cargaHr) {
		this.cargaHr = cargaHr;
	}
	
	

}
