

package br.com.fatec.cadastro.view;


import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import br.com.fatec.cadastro.business.DisciplinaBusiness;
import br.com.fatec.cadastro.business.DisciplinaBusiness.MensagemEnum;
import br.com.fatec.cadastro.model.Disciplina;
import br.com.fatec.cadastro.util.JSFUtil;

@ManagedBean
@ViewScoped
public class DisciplinaMB implements Serializable{

	private static final long serialVersionUID = 1L;
	private Disciplina disciplina = new Disciplina();
	DisciplinaBusiness db = new DisciplinaBusiness();

	public Disciplina getDisciplina() {
		return disciplina;
	}

	public void setDisciplina(Disciplina disciplina) {
		this.disciplina= disciplina;
	}

	public void salvar() {
			
		if(db.salvarDisciplina(disciplina) == MensagemEnum.SUCESSO){
			System.out.println("Sucesso!");	
			disciplina = new Disciplina();
		}
		else{
			System.out.println("Erro!");
		}
		
		JSFUtil.addMessage("br.com.fatec.cadastro.controller.DisciplinaMB.idMensagem",db.getMsg());
		
		
	}
	
	public List<Disciplina> getDisciplinas()
	{
		 return db.getAllDisciplinas();
		
	}

}
