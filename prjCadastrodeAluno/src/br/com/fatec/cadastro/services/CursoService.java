
package br.com.fatec.cadastro.services;

import java.util.List;



import br.com.fatec.cadastro.model.Aluno;
import br.com.fatec.cadastro.model.Curso;
import br.com.fatec.cadastro.persistence.AlunoDAO;
import br.com.fatec.cadastro.persistence.CursoDAO;
import br.com.fatec.cadastro.persistence.MyEntityManager;

public class CursoService {
	
	private CursoDAO cursoDAO;
	private MyEntityManager myEntityManager;
	
	
	public CursoService()
	{
		this.myEntityManager = new MyEntityManager();
		cursoDAO = new CursoDAO(myEntityManager.getEntityManager());
		
	}
	
	 public boolean save(Curso curso){
		    boolean retorno = false;
		    
	        try{
	            myEntityManager.beginTransaction();
	            cursoDAO.save(curso);
	            myEntityManager.commit();
	            retorno = true;
	        }catch(Exception e){
	            e.printStackTrace();
	            myEntityManager.rollBack();
	            
	        }
	        finally
	        {
	        	myEntityManager.close();
	        	
	        }
	        
	        return retorno;
	    }

	public List<Curso> getAllCursos() {
		
		return cursoDAO.findAll(Curso.class);
		
	}

}









