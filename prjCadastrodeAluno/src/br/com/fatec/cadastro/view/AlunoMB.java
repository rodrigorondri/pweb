

package br.com.fatec.cadastro.view;


import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import br.com.fatec.cadastro.business.AlunoBusiness;
import br.com.fatec.cadastro.business.AlunoBusiness.MensagemEnum;
import br.com.fatec.cadastro.model.Aluno;
import br.com.fatec.cadastro.util.JSFUtil;

@ManagedBean
@ViewScoped
public class AlunoMB implements Serializable{

	private static final long serialVersionUID = 1L;
	private Aluno aluno = new Aluno();
	AlunoBusiness ab = new AlunoBusiness();

	public Aluno getAluno() {
		return aluno;
	}

	public void setAluno(Aluno aluno) {
		this.aluno= aluno;
	}

	public void salvar() {
			
		if(ab.salvarAluno(aluno) == MensagemEnum.SUCESSO){
			System.out.println("Sucesso!");	
			aluno = new Aluno();
		}
		else{
			System.out.println("Erro!");
		}
		
		JSFUtil.addMessage("br.com.fatec.cadastro.controller.AlunoMB.idMensagem",ab.getMsg());
		
		
	}
	
	public List<Aluno> getAlunos()
	{
		 return ab.getAllAlunos();
		
	}

}
