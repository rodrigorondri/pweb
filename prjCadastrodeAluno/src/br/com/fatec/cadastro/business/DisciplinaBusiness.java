
package br.com.fatec.cadastro.business;


import java.util.List;

import br.com.fatec.cadastro.model.Disciplina;
import br.com.fatec.cadastro.services.DisciplinaService;

public class DisciplinaBusiness {
	
	
	public enum MensagemEnum { SUCESSO, ERROR };
	private String msg;

	public MensagemEnum salvarDisciplina(Disciplina disciplina)
	{

	    MensagemEnum retorno;
		
		if(disciplina == null){
			msg= "br.com.fatec.cadastro.business.disciplinaNullError";
			retorno = MensagemEnum.ERROR;
		}
		else
		{
			if(disciplina.getNome().length() < 2)
			{
				msg = "br.com.fatec.cadastro.business.cursoSizeError";
				retorno = MensagemEnum.ERROR;
			}
			else
			{
				if(disciplina.getCodigoDis() < 1)
				{
					msg = "br.com.fatec.cadastro.business.erroCodigo";
					retorno = MensagemEnum.ERROR;
				}
				else
				{
					   
					   
					DisciplinaService disciplinaService = new DisciplinaService();
					   if(disciplinaService.save(disciplina))
					   {
					      retorno = MensagemEnum.SUCESSO;
					      msg =  "br.com.fatec.cadastro.business.disciplinaSucesso";
					   }
					   else
					   {
						  retorno = MensagemEnum.ERROR;
						  msg =  "br.com.fatec.cadastro.business.erroBanco";
					   }
				}
			}
		}
		
	   return retorno;		
			
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public List<Disciplina> getAllDisciplinas() {
		 
		DisciplinaService disciplinaService = new DisciplinaService();
		return disciplinaService.getAllDisciplinas();
	}

}
